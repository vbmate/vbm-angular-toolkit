/**
 * @name loggerToastrDemo
 * @namespace Modules
 * @desc Demonstration for logger extension (based on angular-toastr).
 */
(function () {
    'use strict';

    angular
        .module('loggerToastrDemo', ['vbm.logger', 'toastr']);
})();
