/**
 * Login Service
 * @namespace Services
 */
(function () {
    'use strict';

    angular
        .module('vbm.login')
        .factory('login', loginService);

    loginService.$inject = [
        '$http',
        '$q',
        '$interval'
    ];

    /**
     * @namespace loginService
     * @desc Service provider for login processes
     * @memberOf Services
     * @returns {loginService}
     * @constructor
     */
    function loginService($http, $q, $interval) {
        var authenticationMethods = {
            BASIC_AUTHENTICATION:      0,
            FORM_BASED_AUTHENTICATION: 1
        };

        var authenticated = false;
        var runningInterval = undefined;
        var interval = -1;
        var loginUrl = 'login';
        var logoutUrl = 'logout';
        var userInfoUrl = 'user';
        var userAttribute = 'user';
        var authenticationMethod = authenticationMethods.BASIC_AUTHENTICATION;
        var userInfo = undefined;

        var api = {
            authenticationMethods: authenticationMethods,

            init:                    init,
            setInterval:             setInterval,
            setLoginUrl:             setLoginUrl,
            setLogoutUrl:            setLogoutUrl,
            setUserInfoUrl:          setUserInfoUrl,
            setAuthenticationMethod: setAuthenticationMethod,
            login:                   login,
            logout:                  logout,
            isAuthenticated:         isAuthenticated,
            getUserInfo:             getUserInfo
        };

        return api;

        ////////////////////////////////////////////////////////////////////////////////

        /**
         * Initalizes the login service according to the given configuration.
         *
         * @param {object} newConfig  the configuration to use
         */
        function init(newConfig) {
            if (newConfig) {
                setInterval(newConfig.interval);
                setLoginUrl(newConfig.loginUrl);
                setLogoutUrl(newConfig.logoutUrl);
                setUserInfoUrl(newConfig.userInfoUrl);
                setAuthenticationMethod(newConfig.authenticationMethod);
            }
        }

        /**
         * Sets the interval to the given value (in milliseconds).
         *
         * @param {long} newInterval  interval in milliseconds (-1 means no iterations)
         */
        function setInterval(newInterval) {
            interval = newInterval;
            if (runningInterval) {
                $interval.cancel(runningInterval);
                runningInterval = undefined;
            }
            if (interval > 0) {
                runningInterval = $interval(login, interval);
            }
        }

        /**
         * Sets the login url to the given one.
         *
         * @param {string} newLoginUrl  login url to use
         */
        function setLoginUrl(newLoginUrl) {
            if (newLoginUrl) {
                loginUrl = newLoginUrl;
            }
        }

        /**
         * Sets the logout url to the given one.
         *
         * @param {string} newLogoutUrl  logout url to use
         */
        function setLogoutUrl(newLogoutUrl) {
            if (newLogoutUrl) {
                logoutUrl = newLogoutUrl;
            }
        }

        /**
         * Sets the user info url to the given one.
         *
         * @param {string} newUserInfoUrl  user info url to use
         */
        function setUserInfoUrl(newUserInfoUrl) {
            if (newUserInfoUrl) {
                userInfoUrl = newUserInfoUrl;
            }
        }

        /**
         * Sets the authentication method to use.
         *
         * @param {int} authenticationMethod  the authentication method to use (authenticationMethods.BASIC_AUTHENTICATION, authenticationMethods.FORM_BASED_AUTHENTICATION)
         */
        function setAuthenticationMethod(newAuthenticationMethod) {
            if (newAuthenticationMethod) {
                authenticationMethod = newAuthenticationMethod;
            }
        }

        /**
         * Gathers user information.
         *
         * @return {promise} the promise of the information gathering call
         */
        function gatherUserInfo() {
            return $http
                .get(userInfoUrl)
                .then(infoGathered)
                .catch(infoGatheringFailed);

            function infoGathered(info) {
                authenticated = true;
                userInfo = info.data;
                return userInfo;
            }

            function infoGatheringFailed() {
                authenticated = false;
            }
        }

        /**
         * Conducts a login process.
         *
         * @param {object} credentials  credentials containing 'username' and 'password'
         *
         * @return {promise} the promise of the login call
         */
        function login(credentials) {
            var result = undefined;

            switch (authenticationMethod) {
                case authenticationMethods.BASIC_AUTHENTICATION:
                    result = basicAuthentication(credentials, loginComplete, loginFailed);
                    break;
                case authenticationMethods.FORM_BASED_AUTHENTICATION:
                    result = formBasedAuthentication(credentials, loginComplete, loginFailed);
                    break;
                default:
                    result = basicAuthentication(credentials, loginComplete, loginFailed);
                    break;
            }

            return result;

            function loginComplete() {
                return gatherUserInfo();
            }

            function loginFailed(error) {
                authenticated = false;
                userInfo = undefined;
                return $q.reject(error);
            }
        }

        /**
         * Provides BASIC authentication with the given credentials and login handler functions.
         *
         * @param {object}   credentials    credentials containing 'username' and 'password'
         * @param {function} loginComplete  login complete handler
         * @param {function} loginFailed    login failure handler
         *
         * @return {promise} the promise of the login call
         */
        function basicAuthentication(credentials, loginComplete, loginFailed) {
            var headers = new Object({});
            if (credentials) {
                headers['authorization'] = "Basic " + btoa(credentials.username + ":" + credentials.password);
            }

            return $http
                .post(loginUrl, {}, {headers: headers})
                .then(loginComplete)
                .catch(loginFailed);
        }

        /**
         * Provides form based authentication with the given credentials and login handler functions.
         *
         * @param {object}   credentials    credentials containing 'username' and 'password'
         * @param {function} loginComplete  login complete handler
         * @param {function} loginFailed    login failure handler
         *
         * @return {promise} the promise of the login call
         */
        function formBasedAuthentication(credentials, loginComplete, loginFailed) {
            var parameters = new Object({});
            if (credentials) {
                parameters.username = credentials.username;
                parameters.password = credentials.password;
            }

            return $http
                .post(loginUrl, parameters)
                .then(loginComplete)
                .catch(loginFailed);
        }

        /**
         * Conducts a logout process.
         *
         * @return {promise} the promise of the logout call
         */
        function logout() {
            if (authenticated) {
                return $http
                    .post(logoutUrl)
                    .then(logoutComplete);
            }

            function logoutComplete(response) {
                authenticated = false;
                userInfo = undefined;
                return response.data;
            }
        }

        /**
         * Returns if the user has been authenticated.
         *
         * @return {boolean} flag showing if the user is authenticated
         */
        function isAuthenticated() {
            return authenticated;
        }

        /**
         * Returns the user info returned by server.
         *
         * @return {object} the user information
         */
        function getUserInfo() {
            return userInfo;
        }
    }
})();
