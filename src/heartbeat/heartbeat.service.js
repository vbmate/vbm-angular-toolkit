/**
 * Heartbeat Service
 * @memberOf vbmAngularToolkit
 * @namespace Services
 */
(function () {
    'use strict';

    angular
        .module('vbm.heartbeat')
        .factory('heartbeat', heartbeat);

    heartbeat.$inject = [
        '$http',
        '$interval'
    ];

    /**
     * @namespace heartbeat
     * @desc Heartbeat service for maintaining a live connection with server
     * @memberOf Services
     * @returns {heartbeat}
     * @constructor
     */
    function heartbeat($http, $interval) {
        var api = {
            init:                 init,
            start:                start,
            stop:                 stop,
            setUrl:               setUrl,
            setInterval:          setInterval,
            setResponseValidator: setResponseValidator,
            isAlive:              isAlive
        };

        var config = {
            url:               'favicon.ico',
            interval:          30000,
            responseValidator: function () {
                return true;
            }
        };

        var runningInterval;
        var alive = true;

        return api;

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * Initalizes the heartbeat service according to the given configuration.
         *
         * @param {object} newConfig  the configuration to use
         */
        function init(newConfig) {
            if (newConfig) {
                config.url = newConfig.url ? newConfig.url : config.url;
                config.interval = newConfig.interval ? newConfig.interval : config.interval;
                config.responseValidator = newConfig.responseValidator ? newConfig.responseValidator : config.responseValidator;
            }

            start();
        }

        /**
         * Starts the service by registering the periodic check.
         */
        function start() {
            // countermeasure for multiple consecutive calls
            stop();

            runningInterval = $interval(heartbeatCheck, config.interval);
        }

        /**
         * Stopping the service by unregistering the periodic check.
         *
         * @return {boolean} whether the periodic calls were on in the first place
         */
        function stop() {
            var result = angular.isDefined(runningInterval);
            if (runningInterval) {
                $interval.cancel(runningInterval);
                runningInterval = undefined;
            }
            return result;
        }

        /**
         * Checks the connection using the given configuration.
         *
         * @private
         */
        function heartbeatCheck() {
            $http
                .get(config.url)
                .then(function (response) {
                    checkState(
                        config.responseValidator(response.data),
                        config.onCut,
                        config.onReconnected);
                })
                .catch(function () {
                    checkState(false, config.onCut, config.onReconnected);
                });
        }

        /**
         * Checks the new state against the existing one and calls the appropriate callback function.
         *
         * @param {boolean}  newValue       connection state flag
         * @param {function} onCut          function to call in case the connection got lost
         * @param {function} onReconnected  function to call in case the connection got reestablished
         * @private
         */
        function checkState(newValue, onCut, onReconnected) {
            if (alive && !newValue) {
                onCut && onCut();
            }
            if (!alive && newValue) {
                onReconnected && onReconnected();
            }
            alive = newValue;
        }

        /**
         * Sets the given url to call from now on.
         *
         * @param {string} url  the url to set
         */
        function setUrl(url) {
            config.url = url;
        }

        /**
         * Sets the given interval to call from now on.
         *
         * @param {number} interval  the interval to set
         */
        function setInterval(interval) {
            var restartNeeded = stop();
            config.interval = interval;
            if (restartNeeded) {
                start();
            }
        }

        /**
         * Sets the given function to validate responses.
         *
         * @param {function} validator  the validator to set
         */
        function setResponseValidator(validator) {
            config.responseValidator = validator;
        }

        /**
         * @desc Returns if the connection with the server is alive.
         *
         * @returns {boolean} alive flag
         */
        function isAlive() {
            return alive;
        }
    }
})();

