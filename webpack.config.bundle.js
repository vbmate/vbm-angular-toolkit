var webpack = require('webpack');

module.exports = {
    entry:   {
        "vbmAngularToolkit": [
            "./src/heartbeat/heartbeat.module",
            "./src/heartbeat/heartbeat.service",
            "./src/logger/logger.module",
            "./src/logger/logger.service",
            "./src/login/login.module",
            "./src/login/login.service"
        ]
    },
    output:  {
        path:     "./dist",
        filename: "[name].min.js"
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false}
        })
    ]
};
