/**
 * Logger Toastr Service
 * @memberOf loggerToastrDemo
 * @namespace Services
 */
(function () {
    'use strict';

    angular
        .module('loggerToastrDemo')
        .factory('loggerToastr', loggerToastr);

    loggerToastr.$inject = [
        'logger',
        'toastr'
    ];

    /**
     * @namespace loggerToastr
     * @desc Logger extension using toastr for better user experience.
     * @memberOf Services
     * @returns {logger}
     * @constructor
     */
    function loggerToastr(logger, toastr) {
        var api = {
            init:                 init,
            setDebugParameters:   setDebugParameters,
            setInfoParameters:    setInfoParameters,
            setSuccessParameters: setSuccessParameters,
            setWarningParameters: setWarningParameters,
            setErrorParameters:   setErrorParameters,
            setFatalParameters:   setFatalParameters,
            setLevelParameters:   setLevelParameters,
            setLogLevel:          setLogLevel
        };

        var logLevel = logger.logLevels.INFO;
        var config = {
            0: {
                timeOut:     0,
                progressBar: false
            },
            1: {
                timeOut:     10000,
                progressBar: true
            },
            2: {
                timeOut:     10000,
                progressBar: true
            },
            3: {
                timeOut:     10000,
                progressBar: true
            },
            4: {
                timeOut:     10000,
                progressBar: true
            },
            5: {
                timeOut:     0,
                progressBar: false
            }
        };

        return api;

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * Initalizes and registers the toastr based logger extension.
         *
         * @param {object} newConfig  the configuration to use
         */
        function init(newConfig) {
            if (newConfig) {
                setLevelParameters(logger.logLevels.DEBUG, newConfig.DEBUG);
                setLevelParameters(logger.logLevels.INFO, newConfig.INFO);
                setLevelParameters(logger.logLevels.SUCCESS, newConfig.SUCCESS);
                setLevelParameters(logger.logLevels.WARNING, newConfig.WARNING);
                setLevelParameters(logger.logLevels.ERROR, newConfig.ERROR);
                setLevelParameters(logger.logLevels.FATAL, newConfig.FATAL);
            }

            logger.init({
                extensions: {
                    0: debug,
                    1: info,
                    2: success,
                    3: warning,
                    4: error,
                    5: fatal
                }
            })
        }

        /**
         * Sets the level of logging.
         *
         * @param {number} level  the new log level
         */
        function setLogLevel(level) {
            logLevel = level;
        }

        /**
         * Sets the given parameters for the DEBUG level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setDebugParameters(parameters) {
            setLevelParameters(logger.logLevels.DEBUG, parameters);
        }

        /**
         * Sets the given parameters for the INFO level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setInfoParameters(parameters) {
            setLevelParameters(logger.logLevels.INFO, parameters);
        }

        /**
         * Sets the given parameters for the SUCCESS level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setSuccessParameters(parameters) {
            setLevelParameters(logger.logLevels.SUCCESS, parameters);
        }

        /**
         * Sets the given parameters for the WARNING level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setWarningParameters(parameters) {
            setLevelParameters(logger.logLevels.WARNING, parameters);
        }

        /**
         * Sets the given parameters for the ERROR level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setErrorParameters(parameters) {
            setLevelParameters(logger.logLevels.ERROR, parameters);
        }

        /**
         * Sets the given parameters for the FATAL level.
         *
         * @param {object} parameters  the parameters to set
         */
        function setFatalParameters(parameters) {
            setLevelParameters(logger.logLevels.FATAL, parameters);
        }

        /**
         * Sets the given parameters for the specified level.
         *
         * @param {number} level       the level to set parameters for
         * @param {object} parameters  the parameters to set
         */
        function setLevelParameters(level, parameters) {
            if (parameters) {
                config[level].timeOut = parameters.timeOut ? parameters.timeOut : config[level].timeOut;
                config[level].progressBar = parameters.progressBar ? parameters.progressBar : false;
            }
        }

        /**
         * Logs debug messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function debug(message, data, title) {
            if (logLevel <= logger.logLevels.DEBUG) {
                toastr.info(message, 'DEBUG - ' + title, {
                    timeOut:     config[logger.logLevels.DEBUG].timeOut,
                    progressBar: config[logger.logLevels.DEBUG].progressBar
                });
            }
        }

        /**
         * Logs info messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function info(message, data, title) {
            if (logLevel <= logger.logLevels.INFO) {
                toastr.info(message, title, {
                    timeOut:     config[logger.logLevels.INFO].timeOut,
                    progressBar: config[logger.logLevels.INFO].progressBar
                });
            }
        }

        /**
         * Logs success messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function success(message, data, title) {
            if (logLevel <= logger.logLevels.SUCCESS) {
                toastr.success(message, title, {
                    timeOut:     config[logger.logLevels.SUCCESS].timeOut,
                    progressBar: config[logger.logLevels.SUCCESS].progressBar
                });
            }
        }

        /**
         * Logs debug messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function warning(message, data, title) {
            if (logLevel <= logger.logLevels.WARNING) {
                toastr.warning(message, title, {
                    timeOut:     config[logger.logLevels.WARNING].timeOut,
                    progressBar: config[logger.logLevels.WARNING].progressBar
                });
            }
        }

        /**
         * Logs errors with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function error(message, data, title) {
            if (logLevel <= logger.logLevels.ERROR) {
                toastr.error(message, title, {
                    timeOut:     config[logger.logLevels.ERROR].timeOut,
                    progressBar: config[logger.logLevels.ERROR].progressBar
                });
            }
        }

        /**
         * Logs fatal errors with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function fatal(message, data, title) {
            if (logLevel <= logger.logLevels.FATAL) {
                toastr.error(message, title, {
                    timeOut:     config[logger.logLevels.FATAL].timeOut,
                    progressBar: config[logger.logLevels.FATAL].progressBar
                });
            }
        }
    }
})();

