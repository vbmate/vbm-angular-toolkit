/**
 * @name heartbeat
 * @namespace Modules
 * @desc Heartbeat module
 */
(function () {
    'use strict';

    angular
        .module('vbm.heartbeat', []);
})();
