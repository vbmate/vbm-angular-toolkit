describe('logger service - toastr extension', function () {
    var instance;
    var logger;
    var toastr;

    beforeEach(module('vbm.logger'));
    beforeEach(module('loggerToastrDemo'));

    beforeEach(inject(function ($injector) {
        instance = $injector.get('loggerToastr');
        logger = $injector.get('logger');
        toastr = $injector.get('toastr');
    }));

    describe('smoke test', function () {
        it('service exists and injected', function () {
            expect(instance).to.exist;
        });
    });

    describe('DEBUG extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'info');
            instance.init();
            logger.setLogLevel(logger.logLevels.DEBUG);
            instance.setLogLevel(logger.logLevels.DEBUG);
            logger.debug('test message', null, 'test title');
            expect(call).calledWith('test message', 'DEBUG - test title', {
                timeOut:     0,
                progressBar: false
            });
        });
    });

    describe('INFO extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'info');
            instance.init();
            logger.info('test message', null, 'test title');
            expect(call).calledWith('test message', 'test title', {
                timeOut:     10000,
                progressBar: true
            });
        });
    });

    describe('SUCCESS extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'success');
            instance.init();
            logger.success('test message', null, 'test title');
            expect(call).calledWith('test message', 'test title', {
                timeOut:     10000,
                progressBar: true
            });
        });
    });

    describe('WARNING extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'warning');
            instance.init();
            logger.warning('test message', null, 'test title');
            expect(call).calledWith('test message', 'test title', {
                timeOut:     10000,
                progressBar: true
            });
        });
    });

    describe('ERROR extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'error');
            instance.init();
            logger.error('test message', null, 'test title');
            expect(call).calledWith('test message', 'test title', {
                timeOut:     10000,
                progressBar: true
            });
        });
    });

    describe('FATAL extension', function () {
        it('works', function () {
            var call = sinon.spy(toastr, 'error');
            instance.init();
            logger.fatal('test message', null, 'test title');
            expect(call).calledWith('test message', 'test title', {
                timeOut:     0,
                progressBar: false
            });
        });
    });

    describe('configuration', function () {
        it('at initialization time', function () {
            var call = sinon.spy(toastr, 'info');
            instance.init({
                DEBUG: {
                    timeOut: 10000
                },
                INFO:  {
                    timeOut:     5000,
                    progressBar: false
                }
            });
            logger.setLogLevel(logger.logLevels.DEBUG);
            instance.setLogLevel(logger.logLevels.DEBUG);
            logger.debug('test message', null, 'test title');
            logger.info('test message 2', null, 'test title 2');
            expect(call).calledWith('test message', 'DEBUG - test title', {
                timeOut:     10000,
                progressBar: false
            });
            expect(call).calledWith('test message 2', 'test title 2', {
                timeOut:     5000,
                progressBar: false
            });
        });

        it('with high-level functions', function () {
            var call = sinon.spy(toastr, 'info');
            instance.init();
            instance.setDebugParameters({
                timeOut: 10000
            });
            instance.setInfoParameters({
                timeOut:     5000,
                progressBar: false
            });
            logger.setLogLevel(logger.logLevels.DEBUG);
            instance.setLogLevel(logger.logLevels.DEBUG);
            logger.debug('test message', null, 'test title');
            logger.info('test message 2', null, 'test title 2');
            expect(call).calledWith('test message', 'DEBUG - test title', {
                timeOut:     10000,
                progressBar: false
            });
            expect(call).calledWith('test message 2', 'test title 2', {
                timeOut:     5000,
                progressBar: false
            });
        });

        it('with low-level functions', function () {
            var call = sinon.spy(toastr, 'info');
            instance.init();
            instance.setLevelParameters(logger.logLevels.DEBUG, {
                timeOut: 10000
            });
            instance.setLevelParameters(logger.logLevels.INFO, {
                timeOut:     5000,
                progressBar: false
            });
            logger.setLogLevel(logger.logLevels.DEBUG);
            instance.setLogLevel(logger.logLevels.DEBUG);
            logger.debug('test message', null, 'test title');
            logger.info('test message 2', null, 'test title 2');
            expect(call).calledWith('test message', 'DEBUG - test title', {
                timeOut:     10000,
                progressBar: false
            });
            expect(call).calledWith('test message 2', 'test title 2', {
                timeOut:     5000,
                progressBar: false
            });
        });
    });

    describe('log level setting', function () {
        it('setting with level functions works', function () {
            var call_info = sinon.spy(toastr, 'info');
            var call_success = sinon.spy(toastr, 'success');
            var call_warning = sinon.spy(toastr, 'warning');
            var call_error = sinon.spy(toastr, 'error');
            instance.init();
            logger.setLogLevel(logger.logLevels.DEBUG);
            instance.setLogLevel(logger.logLevels.DEBUG);
            logger.debug('test message', null, 'test title');
            logger.setLogLevel(logger.logLevels.INFO);
            logger.debug('test message 2', null, 'test title 2');
            logger.setLogLevel(logger.logLevels.SUCCESS);
            logger.info('test message 3', null, 'test title 3');
            logger.setLogLevel(logger.logLevels.WARNING);
            logger.success('test message 4', null, 'test title 4');
            logger.setLogLevel(logger.logLevels.ERROR);
            logger.warning('test message 5', null, 'test title 5');
            logger.setLogLevel(logger.logLevels.FATAL);
            logger.error('test message 6', null, 'test title 6');
            logger.fatal('test message 7', null, 'test title 7');
            expect(call_info).calledOnce;
            expect(call_info).calledWith('test message', 'DEBUG - test title', {
                timeOut:     0,
                progressBar: false
            });
            expect(call_success).not.called;
            expect(call_warning).not.called;
            expect(call_error).calledOnce;
            expect(call_error).calledWith('test message 7', 'test title 7', {
                timeOut:     0,
                progressBar: false
            });
        });
    });
});
