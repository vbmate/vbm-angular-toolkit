# AngularJS (1) UI Toolkit

This project aims to provide a set of tools for [AngularJS (1)][angular] projects. Originally did to satisfy my oen needs but
of course if anybody else can benefit from it as well, all the better.

The following items are currently in (new ones may come later as needed):
* heartbeat to keep and check connection to the server

## How to use

### Install the package either with [npm][npm] or [bower][bower]:

```
npm install vbm-angular-toolkit
```

or

```
bower install vbm-angular-toolkit
```

### Add the script or scripts to your HTML file

In case you are you use the entire bundle, add

```
<script src="npm_modules/vbm-angular-toolkit/dist/vbmAngularToolkit.min.js"></script>
```

but in case you need only parts of it you can minimize your load by adding any of the followings separately

```
<script src="npm_modules/vbm-angular-toolkit/dist/heartbeat.min.js"></script>
```

```
<script src="npm_modules/vbm-angular-toolkit/dist/logger.min.js"></script>
```

```
<script src="npm_modules/vbm-angular-toolkit/dist/login.min.js"></script>
```

only.

### Use the service you need

#### heartbeat service

```
angular.module('myApp', ['vbm.heartbeat']);
```

```
angular
    .module('myApp')
    .run(init);
    
init.$inject = ['heartbeat'];

function init(heartbeat) {
    heartbeat.init({
        url: 'heartbeatUrl',                // default is favicon.ico
        interval: 60000,                    // default is 30 000ms = 30s
        responseValidator:                  // default lets all pass
            function (response) {
                return response == 'OK';
            }
    });
    
    // or
    
    heartbeat.init();
    // and anytime later anywhere
    heartbeat.setUrl('heartbeatUrl');
    heartbeat.setInterval(10000);
    heartbeat.setResponseValidator(function (response) {
        return response == 'OK';
    });
    
    // or any combination of the two above
}
```

For stopping or starting the service at any time only use:

```
heartbeat.stop();
```

and

```
heartbeat.start();
```

Be default, after initialization the service is on!

#### logger service

```
angular.module('myApp', ['vbm.logger']);
```

```
angular
    .module('myApp')
    .run(init);
    
init.$inject = ['logger'];

function init(logger) {
    var extensions = new Object({});
    extensions[instance.INFO] = function (message, data, title) {
        // wreak all kind of havoc here
    };
    extensions[instance.FATAL] = [
        function (message, data, title) {
            // wreak all kind of havoc here
        },
        function (message, data, title) {
            // wreak some other kind of havoc here
        }
    ];
    logger.init({
        extensions: extensions
    };
    
    // OR you can use the values of the log level constants directly
    // 0 - DEBUG
    // 1 - INFO
    // 2 - SUCCESS
    // 3 - WARNING
    // 4 - ERROR
    // 5 - FATAL
    
    logger.init({
        extensions: {
            1: function (message, data, title) {
                // wreak all kind of havoc here
            },
            5: [
                function (message, data, title) {
                    // wreak all kind of havoc here
                },
                function (message, data, title) {
                    // wreak some other kind of havoc here
                }
            ]
        }
    };
}

//////////////////////////////////////////////////

angular
    .module('myApp')
    .controller('myController', myController);
    
    
myController.$inject = ['logger'];

function myController(logger) {
    // ...
    logger.setLogLevel(logger.ERROR);
    // ...
    logger.debug('message', problematicObject, 'title');    // by default this log message will not get through
    logger.info('message', problematicObject, 'title');
    logger.success('message', problematicObject, 'title');
    logger.warning('message', problematicObject, 'title');
    logger.error('message', problematicObject, 'title');
    logger.fatal('message', problematicObject, 'title');
    // ...
}
```

##### Logger extension demo - angular-toastr

The project contains a demonstration for 'logger extension'. One based on [angular-toastr][toastr]. This is located in folder

```
demo/logger
```

#### login service

Login service offers as of now 2 authentication methods (BASIC and FORM). Authentication also involves a user information gathering step where the success is measured of the existence of a special attribute.

```
angular.module('myApp', ['vbm.login']);
```

```
angular
    .module('myApp')
    .run(init);
    
init.$inject = ['login'];

function init(login) {
    login.init({
        interval: 60000,                                                             // default is -1ms (no iteration)
        loginUrl: 'login-url',                                                       // default is 'login'
        logoutUrl: 'logout-url',                                                     // default is 'logout'
        userInfoUrl: 'user-info-url',                                                // default is 'user'
        authenticationMethod: login.authenticationMethods.FORM_BASED_AUTHENTICATION  // default is 'login.authenticationMethods.BASIC_AUTHENTICATION'
    });
    
    // or
    
    login.init();
    // and anytime later anywhere
    login.setInterval(10000);
    login.setLoginUrl('loginUrl');
    login.setLogoutUrl('logoutUrl');
    login.setUserInfoUrl('user-info-url');
    login.setAuthenticationMethod(login.authenticationMethods.FORM_BASED_AUTHENTICATION);
    
    // or any combination of the two above
}
```

Use the service for login like this:

```
login.login(credentials)
     .then(loginSuccessful)
     .catch(loginFailed);
     
function loginSuccessful(response) {
...
}

function loginFailed(response) {
...
}
```

for logout like this:

```
login.logout()
     .then(logoutSuccessful);
     
function logoutSuccessful(response) {
...
}
```

for querying if the session is authenticated like this:

```
login.isAuthenticated();
```

and for querying the user info sent by the server:

```
login.getUserInfo();
```

The result is undefined after an unsuccessful authentication attempt or after logout.

## How to get and develop

There are basically three ways to get started:
* use the [git][git] project directly (unless you wish to clone and go on with the code or contribute this would be a bit of an overkill)
* use [npm][npm] package manager
* use [bower][bower] package manager


### Prerequisites

The followings you will need:
* [git][git] if you are to aquire the project via git
* [NodeJS][node] for obtaining tool chain dependencies
* [Bower][bower] for getting code related dependencies

### Cloning via [git][git]

Clone the angular-seed repository using [git][git]:

```
git clone https://bitbucket.org/vbmate/ui-toolkit-angular.git
cd ui-toolkit-angular-seed
```

If you just want to start a new project without the commit history then you can do:

```bash
git clone --depth=1 https://bitbucket.org/vbmate/ui-toolkit-angular.git <your-project-name>
```

The `depth=1` tells git to only pull down one commit worth of historical data.

### Getting with [npm][npm]

Installing the package with [npm][npm] is pretty straightforward

```
npm install vbm-angular-toolkit
```

### Getting with [bower][bower]

Installing the package with [bower][bower] is again fairly simple

```
bower install vbm-angular-toolkit
```

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help us manage and test the application.

* get the tools depended upon via `npm`, the [node package manager][npm].
* get the angular code via `bower`, a [client-side code package manager][bower].

`npm` is preconfigured to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `src/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but this location is changed through the `.bowerrc`
file. Putting it in the `src` folder makes it easier to serve the files by a webserver.*

## Directory Layout

```
dist/                       --> all the latest distribution files
demo/                       --> demo application sketches
  logger/                       --> logger demonstration application
    loggerToastr.service.js         --> logger service extension based on angular-toastr
    loggerToastr.service.spec.js    --> test specification code
    loggerToastrDemo.module.js      --> demo module
src/                        --> all of the source files for the application
  heartbeat/                    --> all heartbeat related files
    heartbeat.module.js             --> module definition
    heartbeat.service.js            --> service code
    heartbeat.service.spec.js       --> test specification code
  logger/                       --> all logger related files
    logger.module.js                --> module definition
    logger.service.js               --> service code
    logger.service.spec.js          --> test specification code
  logom/                        --> all login related files
    login.module.js                 --> module definition
    login.service.js                --> service code
    login.service.spec.js           --> test specification code
karma.conf.js               --> config file for running unit tests with Karma
```

## Testing

There are thorough unit tests there for all the services. End-to-end tests on the other hand make not much sense as this is not a stand
alone application but a bunch of services.

### Running Unit Tests

The package comes preconfigured with unit tests. These are written using [mocha][mocha] and [chai][chai] which we run with the
[Karma Test Runner][karma]. Karma configuration file is also provided as well as the `npm scripts` to run them.

* the configuration is found at `karma.conf.js`
* the unit tests are found next to the code they are testing and are named as `*.spec.js`.

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will sit and watch the source and test files for
changes and then re-run the tests whenever any of them change. This is the recommended strategy; if your unit tests are being run every time
you save a file then you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit. This is useful if you want to check that a particular version of the
code is operating as expected. The project contains a predefined script to do this:

```
npm run test-single-run
```

## Updating Angular

The angular framework library code and tools are acquired through package managers (`npm` and `bower`) you can use these tools instead to
update the dependencies.

You can update the tool dependencies by running:

```
npm update
```

This will find the latest versions that match the version ranges specified in the `package.json` file.

You can update the Angular dependencies by running:

```
bower update
```

This will find the latest versions that match the version ranges specified in the `bower.json` file.

## Links

This will find the latest versions that match the version ranges specified in the `bower.json` file.

## Links

[angular]: http://angularjs.org/
[bower]: http://bower.io/
[chai]: http://chaijs.com/
[git]: http://git-scm.com/
[karma]: https://karma-runner.github.io/
[mocha]: http://mochajs.org/
[node]: https://nodejs.org
[npm]: https://www.npmjs.org/
