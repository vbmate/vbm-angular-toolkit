var webpack = require('webpack');

module.exports = {
    entry:   {
        "logger": [
            "./src/logger/logger.module",
            "./src/logger/logger.service"
        ]
    },
    output:  {
        path:     "./dist",
        filename: "[name].min.js"
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false}
        })
    ]
};
