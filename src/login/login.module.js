/**
 * @name login
 * @namespace Modules
 * @desc Login module
 */
(function () {
    'use strict';

    angular
        .module('vbm.login', []);
})();
