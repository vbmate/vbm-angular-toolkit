var webpack = require('webpack');

module.exports = {
    entry:   {
        "heartbeat": [
            "./src/heartbeat/heartbeat.module",
            "./src/heartbeat/heartbeat.service"
        ]
    },
    output:  {
        path:     "./dist",
        filename: "[name].min.js"
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false}
        })
    ]
};
