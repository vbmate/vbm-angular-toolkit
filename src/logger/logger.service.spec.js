'use strict';

describe('logger service', function () {
    var instance;
    var $log;

    beforeEach(module(function ($logProvider) {
    }));

    beforeEach(module('vbm.logger'));

    beforeEach(inject(function ($injector) {
        instance = $injector.get('logger');
        $log = $injector.get('$log');
    }));

    describe('smoke test', function () {
        it('service exists and injected', function () {
            expect(instance).to.exist;
        });

        it('API is in place', function () {
            expect(instance.logLevels).to.exist;
            expect(instance.init).to.exist;
            expect(instance.setLogLevel).to.exist;
            expect(instance.debug).to.exist;
            expect(instance.info).to.exist;
            expect(instance.success).to.exist;
            expect(instance.warning).to.exist;
            expect(instance.error).to.exist;
            expect(instance.debug).to.exist;
        });
    });

    describe('log levels', function () {
        it('all 6 log levels exist', function () {
            expect(instance.logLevels.DEBUG).to.exist;
            expect(instance.logLevels.INFO).to.exist;
            expect(instance.logLevels.SUCCESS).to.exist;
            expect(instance.logLevels.WARNING).to.exist;
            expect(instance.logLevels.ERROR).to.exist;
            expect(instance.logLevels.FATAL).to.exist;
        });

        it('all 6 log methods exist', function () {
            expect(instance.debug).to.exist;
            expect(instance.info).to.exist;
            expect(instance.success).to.exist;
            expect(instance.warning).to.exist;
            expect(instance.error).to.exist;
            expect(instance.fatal).to.exist;
        });

        it('debug() works but with default log level does not logs', function () {
            instance.debug('test message', {data: 'data'});
            expect($log.debug.logs).to.have.lengthOf(0);
        });

        it('info() works', function () {
            instance.info('test message', {data: 'data'});
            expect($log.info.logs).to.have.lengthOf(1);
            expect($log.info.logs[0]).to.be.eql(['test message', {data: 'data'}]);
        });

        it('success() works', function () {
            instance.success('test message', {data: 'data'});
            expect($log.info.logs).to.have.lengthOf(1);
            expect($log.info.logs[0]).to.be.eql(['SUCCESS: test message', {data: 'data'}]);
        });

        it('warning() works', function () {
            instance.warning('test message', {data: 'data'});
            expect($log.warn.logs).to.have.lengthOf(1);
            expect($log.warn.logs[0]).to.be.eql(['test message', {data: 'data'}]);
        });

        it('error() works', function () {
            instance.error('test message', {data: 'data'});
            expect($log.error.logs).to.have.lengthOf(1);
            expect($log.error.logs[0]).to.be.eql(['ERROR: test message', {data: 'data'}]);
        });

        it('fatal() works', function () {
            instance.fatal('test message', {data: 'data'});
            expect($log.error.logs).to.have.lengthOf(1);
            expect($log.error.logs[0]).to.be.eql(['FATAL: test message', {data: 'data'}]);
        });

        it('setting log level changes logging', function () {
            instance.setLogLevel(instance.logLevels.ERROR);
            instance.debug('test message', {data: 'data'});
            instance.info('test message', {data: 'data'});
            instance.success('test message', {data: 'data'});
            instance.warning('test message', {data: 'data'});
            instance.error('test message', {data: 'data'});
            instance.fatal('test message', {data: 'data'});
            expect($log.debug.logs).to.have.lengthOf(0);
            expect($log.info.logs).to.have.lengthOf(0);
            expect($log.warn.logs).to.have.lengthOf(0);
            expect($log.error.logs).to.have.lengthOf(2);
            expect($log.error.logs[0]).to.be.eql(['ERROR: test message', {data: 'data'}]);
            expect($log.error.logs[1]).to.be.eql(['FATAL: test message', {data: 'data'}]);
        });

        it('fatal call sets log level to logLevels.FATAL', function () {
            instance.fatal('test message', {data: 'data'});
            instance.error('test message', {data: 'data'});
            expect($log.error.logs).to.have.lengthOf(1);
            expect($log.error.logs[0]).to.be.eql(['FATAL: test message', {data: 'data'}]);
        });
    });

    describe('extensions', function () {
        it('DEBUG level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.DEBUG] = function (message, data) {
                $log.debug('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.setLogLevel(instance.logLevels.DEBUG);
            instance.debug('test message', {data: 'data'});
            expect($log.debug.logs).to.have.lengthOf(2);
            expect($log.debug.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.debug.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('INFO level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.INFO] = function (message, data) {
                $log.info('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.info('test message', {data: 'data'});
            expect($log.info.logs).to.have.lengthOf(2);
            expect($log.info.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.info.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('SUCCESS level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.SUCCESS] = function (message, data) {
                $log.info('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.success('test message', {data: 'data'});
            expect($log.info.logs).to.have.lengthOf(2);
            expect($log.info.logs[0]).to.be.eql(['SUCCESS: test message', {data: 'data'}]);
            expect($log.info.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('WARNING level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.WARNING] = function (message, data) {
                $log.warn('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.warning('test message', {data: 'data'});
            expect($log.warn.logs).to.have.lengthOf(2);
            expect($log.warn.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.warn.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('ERROR level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.ERROR] = function (message, data) {
                $log.error('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.error('test message', {data: 'data'});
            expect($log.error.logs).to.have.lengthOf(2);
            expect($log.error.logs[0]).to.be.eql(['ERROR: test message', {data: 'data'}]);
            expect($log.error.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('FATAL level extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.FATAL] = function (message, data) {
                $log.error('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.fatal('test message', {data: 'data'});
            expect($log.error.logs).to.have.lengthOf(2);
            expect($log.error.logs[0]).to.be.eql(['FATAL: test message', {data: 'data'}]);
            expect($log.error.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('all-scale extensions hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.DEBUG] = function (message, data) {
                $log.debug('REPLICA: ' + message, data);
            };
            extensions[instance.logLevels.INFO] = function (message, data) {
                $log.info('REPLICA: ' + message, data);
            };
            extensions[instance.logLevels.SUCCESS] = function (message, data) {
                $log.info('REPLICA: ' + message, data);
            };
            extensions[instance.logLevels.WARNING] = function (message, data) {
                $log.warn('REPLICA: ' + message, data);
            };
            extensions[instance.logLevels.ERROR] = function (message, data) {
                $log.error('REPLICA: ' + message, data);
            };
            extensions[instance.logLevels.FATAL] = function (message, data) {
                $log.error('REPLICA: ' + message, data);
            };
            instance.init({
                extensions: extensions
            });
            instance.setLogLevel(instance.logLevels.DEBUG);
            instance.debug('test message', {data: 'data'});
            instance.info('test message', {data: 'data'});
            instance.success('test message', {data: 'data'});
            instance.warning('test message', {data: 'data'});
            instance.error('test message', {data: 'data'});
            instance.fatal('test message', {data: 'data'});

            expect($log.debug.logs).to.have.lengthOf(2);
            expect($log.debug.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.debug.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.info.logs).to.have.lengthOf(4);
            expect($log.info.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.info.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
            expect($log.info.logs[2]).to.be.eql(['SUCCESS: test message', {data: 'data'}]);
            expect($log.info.logs[3]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.warn.logs).to.have.lengthOf(2);
            expect($log.warn.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.warn.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.error.logs).to.have.lengthOf(4);
            expect($log.error.logs[0]).to.be.eql(['ERROR: test message', {data: 'data'}]);
            expect($log.error.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
            expect($log.error.logs[2]).to.be.eql(['FATAL: test message', {data: 'data'}]);
            expect($log.error.logs[3]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });

        it('all-scale extensions given in array hold', function () {
            var extensions = new Object({});
            extensions[instance.logLevels.DEBUG] = [function (message, data) {
                $log.debug('REPLICA: ' + message, data);
            }];
            extensions[instance.logLevels.INFO] = [function (message, data) {
                $log.info('REPLICA: ' + message, data);
            }];
            extensions[instance.logLevels.SUCCESS] = [function (message, data) {
                $log.info('REPLICA: ' + message, data);
            }];
            extensions[instance.logLevels.WARNING] = [function (message, data) {
                $log.warn('REPLICA: ' + message, data);
            }];
            extensions[instance.logLevels.ERROR] = [function (message, data) {
                $log.error('REPLICA: ' + message, data);
            }];
            extensions[instance.logLevels.FATAL] = [function (message, data) {
                $log.error('REPLICA: ' + message, data);
            }];
            instance.init({
                extensions: extensions
            });
            instance.setLogLevel(instance.logLevels.DEBUG);
            instance.debug('test message', {data: 'data'});
            instance.info('test message', {data: 'data'});
            instance.success('test message', {data: 'data'});
            instance.warning('test message', {data: 'data'});
            instance.error('test message', {data: 'data'});
            instance.fatal('test message', {data: 'data'});

            expect($log.debug.logs).to.have.lengthOf(2);
            expect($log.debug.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.debug.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.info.logs).to.have.lengthOf(4);
            expect($log.info.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.info.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
            expect($log.info.logs[2]).to.be.eql(['SUCCESS: test message', {data: 'data'}]);
            expect($log.info.logs[3]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.warn.logs).to.have.lengthOf(2);
            expect($log.warn.logs[0]).to.be.eql(['test message', {data: 'data'}]);
            expect($log.warn.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);

            expect($log.error.logs).to.have.lengthOf(4);
            expect($log.error.logs[0]).to.be.eql(['ERROR: test message', {data: 'data'}]);
            expect($log.error.logs[1]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
            expect($log.error.logs[2]).to.be.eql(['FATAL: test message', {data: 'data'}]);
            expect($log.error.logs[3]).to.be.eql(['REPLICA: test message', {data: 'data'}]);
        });
    });
});
