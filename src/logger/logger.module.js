/**
 * @name logger
 * @namespace Modules
 * @desc Logger module
 */
(function () {
    'use strict';

    angular
        .module('vbm.logger', []);
})();
