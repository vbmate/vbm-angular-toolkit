'use strict';

describe('login service', function () {
    var instance;
    var $httpBackend;
    var $interval;

    beforeEach(module('vbm.login'));

    beforeEach(inject(function ($injector) {
        instance = $injector.get('login');
        $httpBackend = $injector.get('$httpBackend');
        $interval = $injector.get('$interval');
    }));

    describe('smoke test', function () {
        it('service exists and injected', function () {
            expect(instance).to.exist;
        });
    });

    describe('authentication', function () {
        it('not authenticated initially', function () {
            expect(instance.isAuthenticated()).to.be.false;
        });

        it('authentication with good credentials succeeds', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200);
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('authentication with wrong credentials fails', function () {
            $httpBackend
                .expectPOST('login')
                .respond(401, '');
            instance.login().catch(function () {
            });
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
        });
    });

    describe('logout', function () {
        it('once logged in logout works', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            $httpBackend
                .expectPOST('logout')
                .respond(200, 'logged out');
            instance.logout();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
        });

        it('without login logout takes no effect', function () {
            expect(instance.isAuthenticated()).to.be.false;
            instance.logout();
            expect(instance.isAuthenticated()).to.be.false;
        });
    });

    describe('interval', function () {
        it('set interval (at initialization time) holds', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.init({
                interval: 30000
            });
            $interval.flush(30010);
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('set interval holds', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.init();
            instance.setInterval(30000);
            $interval.flush(30010);
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });
    });

    describe('login URL', function () {
        it('default login URL is "login"', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('set login URL (at initialization time) holds', function () {
            $httpBackend
                .expectPOST('login-url')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.init({
                loginUrl: 'login-url'
            });
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('set login URL holds', function () {
            $httpBackend
                .expectPOST('login-url')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.setLoginUrl('login-url');
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });
    });

    describe('logout URL', function () {
        it('default logout URL is "logout"', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            $httpBackend
                .expectPOST('logout')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.logout();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
        });

        it('set logout URL (at initialization time) holds', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.init({
                logoutUrl: 'logout-url'
            });
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            $httpBackend
                .expectPOST('logout-url')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.logout();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
        });

        it('set logout URL holds', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.setLogoutUrl('logout-url');
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            $httpBackend
                .expectPOST('logout-url')
                .respond(200, {
                    user: {
                        name: 'User'
                    }
                });
            instance.logout();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
        });
    });

    describe('user info', function () {
        it('default user info is "undefined"', function () {
            expect(instance.getUserInfo()).to.be.undefined;
        });

        it('user info is stored', function () {
            var userObject = {
                user:  {
                    name:  'User',
                    email: 'user@email.to'
                },
                roles: 'USER,ADMIN'

            };
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, userObject);
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            expect(instance.getUserInfo()).to.be.eql(userObject);
        });

        it('user info is cleared at logout', function () {
            var userObject = {
                user:  {
                    name:  'User',
                    email: 'user@emai.to'
                },
                roles: 'USER,ADMIN'

            };
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200, userObject);
            instance.login();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
            expect(instance.getUserInfo()).to.be.eql(userObject);
            $httpBackend
                .expectPOST('logout')
                .respond(200, 'logged out');
            instance.logout();
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.false;
            expect(instance.getUserInfo()).to.be.undefined;
        });
    });

    describe('authentication methods', function () {
        it('basic authentication works"', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200);
            instance.setAuthenticationMethod(instance.authenticationMethods.BASIC_AUTHENTICATION);
            instance.login({
                username: 'user',
                password: 'password'
            });
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('form based authentication works"', function () {
            $httpBackend
                .whenPOST('login', function (data) {
                    console.log(angular.toJson(data));
                    return data === '{\"username\":\"user\",\"password\":\"password\"}';
                })
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200);
            instance.setAuthenticationMethod(instance.authenticationMethods.FORM_BASED_AUTHENTICATION);
            instance.login({
                username: 'user',
                password: 'password'
            });
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });

        it('basic authentication is default method"', function () {
            $httpBackend
                .expectPOST('login')
                .respond(200);
            $httpBackend
                .expectGET('user')
                .respond(200);
            instance.login({
                username: 'user',
                password: 'password'
            });
            $httpBackend.flush();
            expect(instance.isAuthenticated()).to.be.true;
        });
    });
});
