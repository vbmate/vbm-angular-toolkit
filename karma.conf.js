//jshint strict: false
module.exports = function (config) {
    config.set({

        basePath: './src',

        files:         [
            'bower_components/angular/angular.min.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-route/angular-route.min.js',
            'bower_components/angular-toastr/dist/angular-toastr.min.js',

            'heartbeat/*.js',
            'logger/*.js',
            'login/*.js',

            '../demo/logger/*.module.js',
            '../demo/logger/*.service.js',
            '../demo/logger/*.service.spec.js'
        ],
        exclude:       [],
        preprocessors: {},

        autoWatch: true,

        frameworks:  ['mocha', 'chai', 'sinon-chai'],
        reporters:   ['html'],
        browsers:    ['PhantomJS'],
        port:        9876,
        singleRun:   true,
        concurrency: Infinity,
        logLevel:    config.LOG_DEBUG,
        colors:      true
    });
};
