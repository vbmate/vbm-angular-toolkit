'use strict';

describe('heartbeat service', function () {
    var instance;
    var $httpBackend;
    var $interval;

    beforeEach(module('vbm.heartbeat'));

    beforeEach(inject(function ($injector) {
        instance = $injector.get('heartbeat');
        $httpBackend = $injector.get('$httpBackend');
        $interval = $injector.get('$interval');
    }));

    describe('smoke test', function () {
        it('service exists and injected', function () {
            expect(instance).to.exist;
        });

        it('API is in place', function () {
            expect(instance.init).to.exist;
            expect(instance.start).to.exist;
            expect(instance.stop).to.exist;
            expect(instance.setUrl).to.exist;
            expect(instance.setInterval).to.exist;
            expect(instance.setResponseValidator).to.exist;
            expect(instance.isAlive).to.exist;
        });
    });

    describe('connection check', function () {
        it('connection is ON initially', function () {
            expect(instance.isAlive()).to.be.true;
        });

        it('no response invalidates connection', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.init();
            $interval.flush(30010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });

    });

    describe('url', function () {
        it('default url is "favicon.ico"', function () {
            $httpBackend
                .expectGET('favicon.ico')
                .respond(200, 'OK');
            instance.init();
            $interval.flush(30010);
            $httpBackend.flush();
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
            expect(instance.isAlive()).to.be.true;
        });

        it('set url holds', function () {
            $httpBackend
                .expectGET('heartbeat')
                .respond(200, 'OK');
            instance.init({
                url: 'heartbeat'
            });
            $interval.flush(30010);
            $httpBackend.flush();
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
            expect(instance.isAlive()).to.be.true;
        });

        it('url set after initialization holds', function () {
            $httpBackend
                .expectGET('heartbeat')
                .respond(200, 'OK');
            instance.init();
            instance.setUrl('heartbeat');
            $interval.flush(30010);
            $httpBackend.flush();
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
            expect(instance.isAlive()).to.be.true;
        });
    });

    describe('interval', function () {
        it('default interval is 30s"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.init();
            $interval.flush(10000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(20010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });

        it('set interval holds"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.init({
                interval: 3000
            });
            $interval.flush(2000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(1010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });

        it('interval set after initialization holds"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.init({
                interval: 3000
            });
            instance.setInterval(10000);
            $interval.flush(2000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(4000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(4010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });
    });

    describe('validation function', function () {
        it('default validation function lets any response pass"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(200, 'HUGE ISSUES');
            instance.init();
            $interval.flush(10000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(20010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.true;
        });

        it('set validation holds"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(200, 'all right');
            instance.init({
                responseValidator: function (response) {
                    return response == 'OK';
                }
            });
            $interval.flush(20000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(10010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false
        });

        it('validator set after initialization holds"', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(200, 'all right');
            instance.init();
            instance.setResponseValidator(function (response) {
                return response == 'OK';
            });
            $interval.flush(20000);
            expect(instance.isAlive()).to.be.true;
            $interval.flush(10010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
            instance.setResponseValidator(function (response) {
                console.log(response);
                return response == 'all right';
            });
            $interval.flush(20000);
            expect(instance.isAlive()).to.be.false;
            $interval.flush(10010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.true;
        });
    });

    describe('switch', function () {
        it('heartbeat is ON by default after initialization', function () {
            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.init();
            $interval.flush(30010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });

        it('heartbeat can be turned OFF', function () {
            instance.init();
            instance.stop();
            $interval.flush(30010);
            expect(instance.isAlive()).to.be.true;
        });

        it('heartbeat can be turned back ON once OFF', function () {
            instance.init();
            instance.stop();
            $interval.flush(30010);
            expect(instance.isAlive()).to.be.true;

            $httpBackend
                .whenGET('favicon.ico')
                .respond(404, '');
            instance.start();
            $interval.flush(30010);
            $httpBackend.flush();
            expect(instance.isAlive()).to.be.false;
        });
    });
});
