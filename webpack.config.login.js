var webpack = require('webpack');

module.exports = {
    entry:   {
        "login": [
            "./src/login/login.module",
            "./src/login/login.service"
        ]
    },
    output:  {
        path:     "./dist",
        filename: "[name].min.js"
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false}
        })
    ]
};
