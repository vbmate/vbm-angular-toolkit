/**
 * Logger Service
 * @memberOf vbmAngularToolkit
 * @namespace Services
 */
(function () {
    'use strict';

    angular
        .module('vbm.logger')
        .factory('logger', logger);

    logger.$inject = [
        '$log'
    ];

    /**
     * @namespace logger
     * @desc Logger service for sophisticated logging.
     * @memberOf Services
     * @returns {logger}
     * @constructor
     */
    function logger($log) {
        var logLevels = {
            DEBUG:   0,
            INFO:    1,
            SUCCESS: 2,
            WARNING: 3,
            ERROR:   4,
            FATAL:   5,
        };

        var api = {
            logLevels: logLevels,

            init:        init,
            setLogLevel: setLogLevel,
            debug:       debug,
            info:        info,
            success:     success,
            warning:     warning,
            error:       error,
            fatal:       fatal
        };

        var logLevel = logLevels.INFO;
        var extensions = {
            0: [],
            1: [],
            2: [],
            3: [],
            4: [],
            5: []
        };

        return api;

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * Initalizes the logger service according to the given configuration.
         *
         * @param {object} newConfig  the configuration to use
         */
        function init(newConfig) {
            if (newConfig) {
                if (newConfig.extensions) {
                    registerExtensions(logLevels.DEBUG);
                    registerExtensions(logLevels.INFO);
                    registerExtensions(logLevels.SUCCESS);
                    registerExtensions(logLevels.WARNING);
                    registerExtensions(logLevels.ERROR);
                    registerExtensions(logLevels.FATAL);
                }
            }

            function registerExtensions(level) {
                if (newConfig.extensions[level]) {
                    if (angular.isArray(newConfig.extensions[level])) {
                        newConfig.extensions[level].forEach(
                            function (extension) {
                                extensions[level].push(extension);
                            });
                    } else {
                        extensions[level].push(newConfig.extensions[level]);
                    }
                }
            }
        }

        /**
         * Sets the level of logging.
         *
         * @param {number} level  the new log level
         */
        function setLogLevel(level) {
            logLevel = level;
        }

        /**
         * Logs debug messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function debug(message, data, title) {
            if (logLevel <= logLevels.DEBUG) {
                $log.debug(message, data);
                extensions[logLevels.DEBUG].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
            }
        }

        /**
         * Logs info messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function info(message, data, title) {
            if (logLevel <= logLevels.INFO) {
                $log.info(message, data);
                extensions[logLevels.INFO].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
            }
        }

        /**
         * Logs success messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function success(message, data, title) {
            if (logLevel <= logLevels.SUCCESS) {
                $log.info('SUCCESS: ' + message, data);
                extensions[logLevels.SUCCESS].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
            }
        }

        /**
         * Logs debug messages with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function warning(message, data, title) {
            if (logLevel <= logLevels.WARNING) {
                $log.warn(message, data);
                extensions[logLevels.WARNING].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
            }
        }

        /**
         * Logs errors with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function error(message, data, title) {
            if (logLevel <= logLevels.ERROR) {
                $log.error('ERROR: ' + message, data);
                extensions[logLevels.ERROR].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
            }
        }

        /**
         * Logs fatal errors with the incoming data.
         *
         * @param {string} message  the message to log
         * @param {object} data     the data the message is about
         * @param {string} title    the title of the message
         */
        function fatal(message, data, title) {
            if (logLevel <= logLevels.FATAL) {
                $log.error('FATAL: ' + message, data);
                extensions[logLevels.FATAL].forEach(
                    function (extension) {
                        extension(message, data, title);
                    });
                logLevel = logLevels.FATAL;
            }
        }
    }
})();

